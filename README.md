# algebraic-geomety

代数構造により表現される位相や空間と、その射影や認識に関する表現や言語の研究。

### References

- [Makinori Ikegami / geometry · GitLab](https://gitlab.com/i-makinori/geometry/)
- [Lisp Tidbits](http://www.thoughtstuff.com/rme/lisp.html)
- [An OpenGL Flight Simulator](https://cs.lmu.edu/~ray/notes/flightsimulator/)

