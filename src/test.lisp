
(progn 
  (ql:quickload :asdf)
  (ql:quickload :cl-opengl)
  (ql:quickload :cl-glut)
  (ql:quickload :cl-glu)
  ;;
  ;; algebra.lisp
)



;; params

(defvar *width* 500)
(defvar *height* 500)
(defvar +counter+ 1)


;;

(defun g2line (x1 y1 x2 y2)
  (gl:with-primitives :lines
    (gl:vertex x1 y1 0.0)
    (gl:vertex x2 y2 0.0)))

(defun g3line (x1 y1 z1 x2 y2 z2)
  (gl:with-primitives :lines
    (gl:vertex x1 y1 z1)
    (gl:vertex x2 y2 z2)))

;;

(defun user-init ())


(defun user-idle ()
  (sleep (/ 1.00 60.00))
  (incf +counter+))

(defun user-display()
  (g2line 10 10 (mod  +counter+ *width*) 100)
  (gl:push-matrix)
  (gl:translate 100 100 0.1)
  (g2line 10 10 (mod  +counter+ *width*) 100)
  (glut:solid-teapot 0.2)
  (gl:pop-matrix)
  (loop for i from 0 to 100
        do (g2line 100 100 
                   (+ 100 (* (sin i) 100))
                   (+ 100 (* (cos i) 100)))))

;;


(defclass simulator-window (glut:window)
  ()  
  (:default-initargs :title "Dynamic low of action and reaction"
                     :width *width* :height *height* :mode '(:double :rgb :depth)))

;;


(defmethod glut:display-window :before ((w simulator-window)))


(defmethod glut:display ((window simulator-window))
  (gl:clear :color-buffer :depth-buffer)

  (gl:shade-model :flat)
  (gl:normal 0 0 0.5)
  
  (user-display)

  (glut:swap-buffers)
)

(defmethod glut:reshape ((window simulator-window) width height)
  (gl:viewport 0 0 width height)
  (gl:load-identity)
  (glu:ortho-2d 0.0 *width* *height* 0.0))

(defmethod glut:keyboard ((window simulator-window) key x y)
  (declare (ignore x y)) 
  (case key
    #|
    (#\w (incf view-rotx spin-speed))
    (#\s (decf view-rotx spin-speed))
    (#\a (incf view-roty spin-speed))
    (#\d (decf view-roty spin-speed))
    ;;
    (#\W (incf view-rotz spin-speed))
    (#\S (decf view-rotz spin-speed))
    (#\A (incf view-rotz spin-speed))
    (#\D (decf view-rotz spin-speed))
    |#
    ;;
    (#\escape
     (glut:destroy-current-window)
     (return-from glut:keyboard))))

(defmethod glut:idle ((window simulator-window))
  (user-idle)
  (glut:post-redisplay))



(defun simulator ()
  (user-init)
  (glut:display-window (make-instance 'simulator-window)))

